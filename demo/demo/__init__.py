from zope.interface import implementer
from pyramid.interfaces import IRootFactory

from pyramid.config import Configurator
from pyramid_theme import ThemeFactory


@implementer(IRootFactory)
class RootFactory(object):
    __name__ = None
    __parent__ = None

    def __call__(self, request):
        self.request = request

        return self


root_factory = RootFactory()


class MyThemeFactory(ThemeFactory):
    @property
    def templates(self):
        return '%s' % self('templates')

    @property
    def images(self):
        return '%s' % self('static/images')

    @property
    def stylesheets(self):
        return '%s' % self('static/stylesheets')

    def template(self, file):
        return '%s/%s' % (self.templates, file)

    def image(self, file):
        return '%s/%s' % (self.images, file)

    def stylesheet(self, file):
        return '%s/%s' % (self.stylesheets, file)


theme_factory = MyThemeFactory('demo:themes/default')


def main(global_config, **settings):
    config = Configurator(
        settings=settings,
        root_factory=root_factory
    )

    config.include('pyramid_mako')
    config.include('pyramid_theme')

    config.set_theme_factory(theme_factory)

    config.add_static_view('images', theme_factory.images)
    config.add_static_view('stylesheets', theme_factory.stylesheets)

    config.scan()

    return config.make_wsgi_app()
