from pyramid.view import view_config, view_defaults

from pyramid.interfaces import IRootFactory

from . import theme_factory as theme


@view_defaults(context=IRootFactory)
class RootViews():
    def __init__(self, context, request):
        self.context = context
        self.request = request

    @view_config(renderer=theme.template('landing.mako'))
    def landing(self):
        return {'project': 'Pyramid Theme Demo'}

    @view_config(name='favicon.ico')
    def favicon(self):
        from pyramid.response import FileResponse
        from pyramid.asset import abspath_from_asset_spec

        return FileResponse(
            abspath_from_asset_spec(theme.image('pyramid-16x16.png')),
            request=self.request,
            content_type='image/png',
            cache_max_age=3600,
        )
