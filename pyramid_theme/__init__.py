from pyramid.events import BeforeRender

from pyramid_theme.interfaces import IThemeFactory
from pyramid_theme.factories import ThemeFactory


def set_theme_factory(config, factory):
    factory = config.maybe_dotted(factory)

    def register():
        config.registry.registerUtility(factory, IThemeFactory)

    intr = config.introspectable('themes factories', None, config.object_description(factory), 'theme factory')
    intr['factory'] = factory
    config.action(IThemeFactory, register, introspectables=(intr,))


def request_method_theme(request):
    theme = request.registry.queryUtility(IThemeFactory)

    if theme is None:
        return lambda x=None: str(x or '')

    return theme


def on_before_render(event):
    event.update({'theme': event.get('request').theme})


def includeme(config):
    config.add_directive('set_theme_factory', set_theme_factory)
    config.add_request_method(request_method_theme, 'theme', property=True)
    config.add_subscriber(on_before_render, BeforeRender)
