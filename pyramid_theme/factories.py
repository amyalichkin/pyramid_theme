from zope.interface import implementer

from pyramid.asset import resolve_asset_spec

from pyramid_theme.interfaces import IThemeFactory


@implementer(IThemeFactory)
class ThemeFactory(object):
    __name__ = None

    def __init__(self, spec):
        self.__name__ = '%s:%s' % resolve_asset_spec(spec)

    def __call__(self, file=None):
        if not file:
            return '%s' % self.__name__

        if isinstance(file, str):
            return '%s/%s' % (self.__name__, file)

        raise ValueError('Only strings')
