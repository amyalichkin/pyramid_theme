from zope.interface import Interface


class IThemeFactory(Interface):
    def __call__(self, file):
        """ Return spec string on file from theme. """
