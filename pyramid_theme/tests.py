import unittest

from pyramid import testing


class ThemeTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def _callFUT(self, *kw, **kwargs):
        from pyramid_theme import ThemeFactory

        return ThemeFactory(*kw, **kwargs)

    def test_theme_factory0(self):
        theme_factory = self._callFUT('spec')

        with self.assertRaises(ValueError) as ex:
            theme_factory(True)
        self.assertEqual('Only strings', str(ex.exception))

    def test_theme_factory1(self):
        theme_factory = self._callFUT('test:theme')

        self.assertEqual('test:theme', theme_factory())
        self.assertEqual('test:theme', theme_factory(''))
        self.assertEqual('test:theme/file', theme_factory('file'))
        self.assertEqual('test:theme/file.ext', theme_factory('file.ext'))

        with self.assertRaises(ValueError):
            theme_factory(True)

    def test_theme_factory2(self):
        theme_factory = self._callFUT('theme')

        self.assertEqual('__main__:theme', theme_factory())
        self.assertEqual('__main__:theme', theme_factory(''))
        self.assertEqual('__main__:theme/file', theme_factory('file'))
        self.assertEqual('__main__:theme/file.ext', theme_factory('file.ext'))

    def test_theme_factory3(self):
        theme_factory = self._callFUT('test:')

        self.assertEqual('test:', theme_factory())
        self.assertEqual('test:', theme_factory(''))
        self.assertEqual('test:/file', theme_factory('file'))
        self.assertEqual('test:/file.ext', theme_factory('file.ext'))

    def test_theme_factory4(self):
        theme_factory = self._callFUT(':theme')

        self.assertEqual(':theme', theme_factory())
        self.assertEqual(':theme', theme_factory(''))
        self.assertEqual(':theme/file', theme_factory('file'))
        self.assertEqual(':theme/file.ext', theme_factory('file.ext'))
