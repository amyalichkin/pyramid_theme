import os

from setuptools import setup, find_packages

VERSION = '0.2'

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid>=1.3',
]

setup(
    name='pyramid_theme',
    version=VERSION,
    description='Pyramid Theme',
    long_description=README + '\n\n' + CHANGES,
    license='MIT',
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'License :: Repoze Public License',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
    author='Alexander Myalichkin',
    author_email='amyalichkin@gmail.com',
    url='https://bitbucket.org/amyalichkin/pyramid_theme',
    keywords='wsgi pylons web pyramid',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    tests_require=requires,
    test_suite='pyramid_theme',
)
